<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('thinkagainlab.frontend.pages.homepage');
});

Route::get('/courses', function(){
      return view('thinkagainlab.frontend.pages.courses');
});

Route::get('/reviews', function(){
      return view('thinkagainlab.frontend.pages.review');
});

Route::get('/signup', function(){
    return view('thinkagainlab.frontend.pages.signup');
});

Route::get('/signin', function(){
    return view('thinkagainlab.frontend.pages.signin');
});

