<!DOCTYPE html>
<html lang="en">
@include('thinkagainlab.frontend.include.headerlinks')
<body>
@include('thinkagainlab.frontend.include.navbar')

@yield('content')

@include('thinkagainlab.frontend.include.footer')     
</body>
@include('thinkagainlab.frontend.include.footerlinks')
</html>