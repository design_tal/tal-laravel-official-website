@extends('thinkagainlab.frontend.layouts.courseslayout')

@section('content')


           <!-- review banner -->
           <div class="container-fluid review-banner text-center text-white">
                  <h1>Real Reviews from Real Students</h1>
                  <hr class="white-hr">
                  <h4>We have 600+ reviews in total. We have collected a sample of the reviews from Facebook below</h4>
            </div>

            <!-- students' reviews -->
            <div class="container-fluid student-review text-center">
                  <hr class="yellow-hr">
                  <h4>Click any review to read more!</h4>
                  <p class="review-screeshot">
                              <a href="https://www.facebook.com/pg/WeArePirple/reviews/" target="_blank"><img src="https://s3.amazonaws.com/thinkific/file_uploads/116598/images/f28/134/3e5/Screenshot_2018-12-07_21.39.53.png" class="fr-fic fr-dib" style="height: 100%; width: 100%;"></a>
                  </p>
                  <p class="review-screeshot">
                              <a href="https://www.facebook.com/pg/WeArePirple/reviews/" target="_blank"><img src="https://s3.amazonaws.com/thinkific/file_uploads/116598/images/f28/134/3e5/Screenshot_2018-12-07_21.39.53.png" class="fr-fic fr-dib" style="height: 100%; width: 100%;"></a>
                  </p>
                  <p class="review-screeshot">
                              <a href="https://www.facebook.com/pg/WeArePirple/reviews/" target="_blank"><img src="https://s3.amazonaws.com/thinkific/file_uploads/116598/images/f28/134/3e5/Screenshot_2018-12-07_21.39.53.png" class="fr-fic fr-dib" style="height: 100%; width: 100%;"></a>
                  </p>
                  <p class="review-screeshot">
                              <a href="https://www.facebook.com/pg/WeArePirple/reviews/" target="_blank"><img src="https://s3.amazonaws.com/thinkific/file_uploads/116598/images/f28/134/3e5/Screenshot_2018-12-07_21.39.53.png" class="fr-fic fr-dib" style="height: 100%; width: 100%;"></a>
                  </p>
                  <p class="review-screeshot">
                              <a href="https://www.facebook.com/pg/WeArePirple/reviews/" target="_blank"><img src="https://s3.amazonaws.com/thinkific/file_uploads/116598/images/f28/134/3e5/Screenshot_2018-12-07_21.39.53.png" class="fr-fic fr-dib" style="height: 100%; width: 100%;"></a>
                  </p>
                  

            </div>    
<!-- Review action banner -->
    <div class="container-fluid review-action-banner text-center text-white">
          <h2>It's time to invest in <strong>yourself! </strong> yoursel!</h2>
          <hr class="white-hr">
          <h4>Pirple.com memberships are designed to give you the skills and knowledge you need to stay competitive as a Software Engineer.</h4>
          <a href="#" class="btn btn-lg rounded-0 btn-think-active btn-think">Okay I'm Ready</a>

    </div>



      













@endsection