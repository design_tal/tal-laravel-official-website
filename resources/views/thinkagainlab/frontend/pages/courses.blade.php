@extends('thinkagainlab.frontend.layouts.courseslayout')

@section('content')


<div class="container-fluid courses-header text-center text-white">
            <h1 class="display-4">Our Courses</h1>
            <hr class="white-hr">
            <h5>We currently offer courses on Javascript (ES6), Node.js, Python, C++, HTML & CSS, as well as Android development (Kotlin), and iOS / MacOS development (Swift). Our newest class (The SaaS Master Class) combines dozens of technologies.</h5>

 </div>

      <!-- Course content Details -->

      <div class="container-fluid course-content">
          <div class="row">
                <div class="col-sm-12 col-lg-6">
                      <div class="course-img left">
                            <img src="{{asset('thinkagainlab/images/courses/course1.png')}}" alt="">

                      </div>
                  </div>
                      <div class="col-sm-12 col-lg-6">
                      <div class="course-details text-left">
                            <h2>The SaaS Master Class</h2>
                            <p>Learn to build an entire tech startup company, from scratch. This year-long class covers research and design, infrastructure setup, CI/CD, backend development, frontend development, marketing, conversion rate optimization, user retention and engagement, scaling, automation, and more.</p>
                            <a href="#" class="btn btn-lg rounded-0 btn-think btn-think-active">VIEW DETAILS</a>

                      </div>
                      </div>

                </div>

            <div class="row">
                <div class="col-sm-12 col-lg-6">
                        <div class="course-details text-left">
                                    <h2>The SaaS Master Class</h2>
                                    <p>Learn to build an entire tech startup company, from scratch. This year-long class covers research and design, infrastructure setup, CI/CD, backend development, frontend development, marketing, conversion rate optimization, user retention and engagement, scaling, automation, and more.</p>
                                    <a href="#" class="btn btn-lg rounded-0 btn-think btn-think-active">VIEW DETAILS</a>
        
                        </div>
                 </div>      
                <div class="col-sm-12 col-lg-6">
                        <div class="course-img right">
                              <img src="{{asset('thinkagainlab/images/courses/course2.png')}}" alt="">
  
                        </div>
                </div>
  
            </div>
                <div class="row">
                        <div class="col-sm-12 col-lg-6">
                              <div class="course-img left">
                                    <img src="{{asset('thinkagainlab/images/courses/course1.png')}}" alt="">
        
                              </div>
                          </div>
                              <div class="col-sm-12 col-lg-6">
                              <div class="course-details text-left">
                                    <h2>The SaaS Master Class</h2>
                                    <p>Learn to build an entire tech startup company, from scratch. This year-long class covers research and design, infrastructure setup, CI/CD, backend development, frontend development, marketing, conversion rate optimization, user retention and engagement, scaling, automation, and more.</p>
                                    <a href="#" class="btn btn-lg rounded-0 btn-think btn-think-active">VIEW DETAILS</a>
        
                              </div>
                              </div>
        
                  </div>
                        
            <div class="row">
                        <div class="col-sm-12 col-lg-6">
                                <div class="course-details text-left">
                                            <h2>The SaaS Master Class</h2>
                                            <p>Learn to build an entire tech startup company, from scratch. This year-long class covers research and design, infrastructure setup, CI/CD, backend development, frontend development, marketing, conversion rate optimization, user retention and engagement, scaling, automation, and more.</p>
                                            <a href="#" class="btn btn-lg rounded-0 btn-think btn-think-active">VIEW DETAILS</a>
                
                                      </div>
                        </div>      
                        <div class="col-sm-12 col-lg-6">
                                <div class="course-img right">
                                      <img src="{{asset('thinkagainlab/images/courses/course2.png')}}" alt="">
          
                                </div>
                        </div>
          
             </div>
      </div>   
            
            
      <!-- courses action banner -->
      <div class="container-fluid action-banner text-center text-white">
            <h2>
                  And that's not it!
            </h2>
            <hr class="white-hr">
            <h4>We'll be releasing new classes every few months for the foreseeable future. When you become a Pirple.com member, you get access to everything we release at no additional cost.</h4>
            <a href="#" class="btn btn-lg rounded-0 btn-think btn-think-active mt-2">JOIN NOW</a>
        
      </div>





@endsection