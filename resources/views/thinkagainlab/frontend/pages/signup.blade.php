@extends('thinkagainlab.frontend.layouts.signuplayout')

@section('content')
 <!-- sign In banner -->

 <div class="container-fluid sign-up-banner text-center mb-4">
            <div class="sign-up-action">
            <div class="sign-up-banner-heading text-white">
                     <h1 class="display-4">Become a Member</h1>
                     <hr class="white-hr">
                  <h4>A pirple.com membership gives you unlimited access to all of our courses, as well as 1-on-1 tutoring, live-chat and email support.</h4>
            </div>
            <div class="sign-up-buttons d-flex flex-row text-center">
               <a href="#" class="btn btn-lg rounded-0 text-white mr-3 btn-think btn-think-active">Yearly Membership</a>
               <a href="#" class="btn btn-lg rounded-0 btn-think btn-think-inactive">Monthly Membership</a>
                  
            </div>
                  
         </div>

 </div>

  <!-- Showcase companies -->

  <div class="container-fluid showcase-companies">
            <div class="showcase-heading text-center">
                  <h2>Train like the best </h2>
                  <hr class="yellow-hr">
                  <h5>Engineers at these companies use Pirple to keep their skills fresh:</h5>
            </div>
            <div class="showcase-images d-flex flex-row justify-content-center align-items-center">
                        <img src="{{asset('thinkagainlab/images/companies/google.png')}}" class="img-responsive" alt="">
                        <img src="{{asset('thinkagainlab/images/companies/Ibm.png')}}" class="img-responsive" alt="">
                        <img src="{{asset('thinkagainlab/images/companies/microsoft.png')}}" class="img-responsive" alt="">
                        <img src="{{asset('thinkagainlab/images/companies/sap.png')}}" class="img-responsive" alt="">
                        <img src="{{asset('thinkagainlab/images/companies/amazon.png')}}" class="img-responsive" alt="">
                  </div>

      </div>

      <!-- Frequently Asked Questions -->

      <div class="container-fluid faqs-section text-left">
         <h2>Frequently Asked Questions</h2>
         <hr class="yellow-hr">
         <h3>If you can't find an answer here, feel free to contact us.</h3>
         <br>
         <div class="question-ans-section">
               <div class="q1">
                     <div class="d-flex flex-row">
                              <i class="fas fa-chevron-right mt-1 mr-2"></i>
                              <div class="question-ans">
                                    <h6 class="question">
                                          Do you have any question?
                                    </h6>
                                    <p class="answer">
                                          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem qui unde itaque velit iusto minus odit adipisci eligendi molestiae eveniet.
                                    </p>
                              </div>
                              
                     </div> 
               </div>
               <!-- question 2 -->
               <div class="q2">
                  <div class="d-flex flex-row">
                           <i class="fas fa-chevron-right mt-1 mr-2"></i>
                           <div class="question-ans">
                                 <h6 class="question">
                                       Do you have any question?
                                 </h6>
                                 <p class="answer">
                                       Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem qui unde itaque velit iusto minus odit adipisci eligendi molestiae eveniet.
                                 </p>
                           </div>
                           
                  </div>
                 
            </div>
            <!-- question 3 -->
            <div class="q3">
            <div class="d-flex flex-row">
                        <i class="fas fa-chevron-right mt-1 mr-2"></i>
                        <div class="question-ans">
                              <h6 class="question">
                                    Do you have any question?
                              </h6>
                              <p class="answer">
                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem qui unde itaque velit iusto minus odit adipisci eligendi molestiae eveniet.
                              </p>
                        </div>
                        
            </div>
            
      </div>
               

         </div>


      </div>
      



@endsection