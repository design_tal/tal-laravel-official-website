@extends('thinkagainlab.frontend.layouts.signinlayout')

@section('content')


      <!-- sign-in email password -->
      <div class="container-fluid welcome-sign-in">
                 <h1 class=" text-center pb-4"><strong>Welcome Back!</strong></h1>
                 <div class="sign-in-details">
                        <form>
                        <div class="form-group">
                              <label for="exampleInputEmail1">Email address</label>
                              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                              <label for="exampleInputPassword1">Password</label>
                              <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div>
                        <div class="clearfix pb-4">
                        <div class="form-check float-left">
                              <input type="checkbox" class="form-check-input" id="exampleCheck1">
                              <label class="form-check-label" for="exampleCheck1">Check me out</label>
                        </div>
                        <div class="forgot-password float-right">
                              <a href="#">Forgot Password</a>
                        </div>
                        </div>
                        <button type="submit" class="btn rounded-0 mb-4 btn-think btn-think-active">Sign In</button>
                        </form>
                 </div>
    <!-- sign in another websites -->
                 <div class="another-sign-in-option text-center">
                 <div class=" mb-4">
                      <hr class="yellow-hr">
                        <h3 class="disabled">Or Sign In with</h3>
                       
                 </div>  
                 <div class="d-lg-flex flex-row justify-content-center mb-2">
                        <a href="#"><i class="fab fa-linkedin-in pl-3 pr-3"></i></a>
                        <a href="#"><i class="fab fa-google-plus-g pr-3"></i></i></a>
                        <a href="#"><i class="fab fa-facebook-f pr-3"></i></a>
                 </div> 
                 <a href="#" class="">Create a new account</a>
            </div> 




           </div>



@endsection