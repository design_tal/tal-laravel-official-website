@extends('thinkagainlab.frontend.layouts.homepagelayout')
@section('content')


            <!-- hero banner -->

            <div class="container-fluid main-page-banner text-center mb-4">
               <div class="heading-action">
               <div class="banner-heading text-white">
                        <h1>Continuing Education for Software Engineers</h1>
                        <hr class="white-hr">
                     <h4 class="text-center ">We offer professional-grade, self-paced courses to help developers stay on top of their craft.</h4>
               </div>
               <div class="buttons d-flex flex-row justify-content-center text-center">
                  <a href="#" class="btn btn-lg rounded-0 text-white mr-3 btn-think btn-think-active">BROWSES THE COURSES</a>
                  <a href="#" class="btn btn-lg rounded-0 btn-think btn-think-inactive">BECOME A MEMBER</a>
                     
               </div>
                     
            </div>

            </div>

            <!-- Showcase companies -->

            <div class="container-fluid showcase-companies">
                  <div class="showcase-heading text-center">
                        <h2>Join a global cohort of <strong>4000+ students</strong> </h2>
                        <hr>
                        <h5>Students in our classes are Engineers at:</h5>
                  </div>
                  <div class="showcase-images d-flex flex-row justify-content-center align-items-center">
                        <img src="{{asset('thinkagainlab/images/companies/google.png')}}" class="img-responsive" alt="">
                        <img src="{{asset('thinkagainlab/images/companies/Ibm.png')}}" class="img-responsive" alt="">
                        <img src="{{asset('thinkagainlab/images/companies/microsoft.png')}}" class="img-responsive" alt="">
                        <img src="{{asset('thinkagainlab/images/companies/sap.png')}}" class="img-responsive" alt="">
                        <img src="{{asset('thinkagainlab/images/companies/amazon.png')}}" class="img-responsive" alt="">
                  </div>

            </div>

            <!-- icon container -->
                <div class="container-fluid icon-container">
                      <div class="icon-container-heading text-center">
                            <h4>
                                    The Pirple.com Difference
                            </h4>
                            <hr>
                            <h5>
                                    What's the difference between taking a class here, vs somewhere else?

                            </h5>
                            <p>Pirple is designed to be more like a real classroom environment. We emphasize homeworks, exams, interacting with your instructors, and working with classmates. Most other online-course providers just give you a bunch of videos to watch. 

                              </p>
                      </div>
                      <div class="container-fluid icon-showcase">
                            <div class="row">
                                  <div class="col-sm-12 col-md-4 cards text-center">
                                          <i class="fas fa-user-friends"></i>
                                          <h6><strong> 24/7 Student
                                                Forum</strong></h6>
                                           <p>Start any topic, and ask anything you want to know related to programming. Anytime!</p>     

                                  </div>
                                  <div class="col-sm-12 col-md-4 cards text-center">
                                          <i class="fas fa-user-friends"></i>
                                          <h6><strong> 24/7 Student
                                                Forum</strong></h4>
                                           <p>Start any topic, and ask anything you want to know related to programming. Anytime!</p>     

                                  </div>
                                  <div class="col-sm-12 col-md-4 cards text-center">
                                          <i class="fas fa-user-friends"></i>
                                          <h6><strong> 24/7 Student
                                                Forum</strong></h4>
                                           <p>Start any topic, and ask anything you want to know related to programming. Anytime!</p>     

                                  </div>
                            </div>
                            <div class="row">
                                    <div class="col-sm-12 col-md-4 cards text-center">
                                            <i class="fas fa-user-friends"></i>
                                            <h6><strong> 24/7 Student
                                                  Forum</strong></h4>
                                             <p>Start any topic, and ask anything you want to know related to programming. Anytime!</p>     
  
                                    </div>
                                    <div class="col-sm-12 col-md-4 cards text-center">
                                            <i class="fas fa-user-friends"></i>
                                            <h6><strong> 24/7 Student
                                                  Forum</strong></h4>
                                             <p>Start any topic, and ask anything you want to know related to programming. Anytime!</p>     
  
                                    </div>
                                    <div class="col-sm-12 col-md-4 cards text-center">
                                            <i class="fas fa-user-friends"></i>
                                            <h6><strong> 24/7 Student
                                                  Forum</strong></h4>
                                             <p>Start any topic, and ask anything you want to know related to programming. Anytime!</p>     
  
                                    </div>
                              </div>
                              <div class="row">
                                    <div class="col-sm-12 col-md-4 cards text-center">
                                                <i class="fas fa-user-friends"></i>
                                                <h6><strong> 24/7 Student
                                                      Forum</strong></h4>
                                                <p>Start any topic, and ask anything you want to know related to programming. Anytime!</p>     
      
                                    </div>
                                    <div class="col-sm-12 col-md-4 cards text-center">
                                                <i class="fas fa-user-friends"></i>
                                                <h6><strong> 24/7 Student
                                                      Forum</strong></h4>
                                                <p>Start any topic, and ask anything you want to know related to programming. Anytime!</p>     
      
                                    </div>
                                    <div class="col-sm-12 col-md-4 cards text-center">
                                                <i class="fas fa-user-friends"></i>
                                                <h6><strong> 24/7 Student
                                                      Forum</strong></h6>
                                                <p>Start any topic, and ask anything you want to know related to programming. Anytime!</p>     
      
                                    </div>
                              </div>
                      </div>
                </div>








@endsection