
<head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">   

      <!-- ---- font awesome link ----- -->
      <script src="https://kit.fontawesome.com/bd08ad0f6e.js"></script>

      <!------- bootstrap link cdn -------->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

       <!-------  google fonts lato   -------->
      <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700&display=swap" rel="stylesheet">

      <!---------  css links  ---------->
      <link type = "text/css" rel="stylesheet" href="{{asset('thinkagainlab/css/style.css')}}">
</head>      